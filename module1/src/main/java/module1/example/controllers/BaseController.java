package module1.example.controllers;

import lombok.extern.slf4j.Slf4j;
import module2.example.Module2Application;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.PostConstruct;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;

import static org.springframework.web.bind.annotation.RequestMethod.POST;

/**
 * Created by kefirkb on 11.01.17.
 */
@Slf4j
@RestController
public class BaseController {
    @Value(value = "${server.message}")
    String message;

    private ConfigurableApplicationContext contextRestartable;

    @Autowired
    private Properties properties;


    @RequestMapping(value = "/add", method = POST)
    @ResponseBody
    public String add(@RequestBody String url) throws IOException {
        log.info("Try to add new " + url + " url mapping!");
        addPropertyUrl(url);
        stopMockServer();
        startMockServer();
        return message;
    }

    private void addPropertyUrl(String url) throws IOException {
        properties.setProperty("urls."+url,url);
        properties.storeToXML(new FileOutputStream("urls_data.xml"),"some comment");
    }

    @PostConstruct
    public void startMockServer() {
        log.info("Try to call start Mock Server!");
        properties.setProperty("server.port", "${other.port}");
        SpringApplication app = Module2Application.getApplication(Module2Application.class);
        app.setDefaultProperties(properties);
        contextRestartable = app.run();
    }


    public void stopMockServer() {
        log.info("Try to call stop Mock Server!");
        contextRestartable.stop();
        contextRestartable.close();
    }

}
