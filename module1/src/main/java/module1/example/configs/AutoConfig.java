package module1.example.configs;

import org.springframework.boot.autoconfigure.web.WebMvcAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

/**
 * Created by ivoronkov on 07.01.2017.
 */

@EnableWebMvc
@Configuration
@ComponentScan
public class AutoConfig extends WebMvcAutoConfiguration {

}
