var url = '/';
var jsonObj = '{"name":"John Rambo", "time":"2pm"}';

var send = function(json, url) {
    var http = new XMLHttpRequest();
    http.open("POST", url);

    http.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
    http.send(JSON.parse(JSON.stringify(json)));
};

//send(jsonObj, url);