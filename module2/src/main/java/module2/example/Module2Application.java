package module2.example;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Module2Application{

	public static void main(String[] args) {
		getApplication(Module2Application.class).run(args);
	}

	public static SpringApplication getApplication(Class<?>... sources) {
		return new SpringApplication(sources);

	}
}
