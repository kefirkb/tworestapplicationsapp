package module2.example.controllers;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import static org.springframework.web.bind.annotation.RequestMethod.GET;

/**
 * Created by sergey on 11.01.17.
 */
@Slf4j
@RestController
public class BaseController {
    @Value(value = "${servermock.message}")
    String message;

    @RequestMapping(value = "/mock", method = GET)
    @ResponseBody
    public String start() {
        log.info("Start Mock Server!");
        return message;
    }

}
