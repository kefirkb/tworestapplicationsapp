package module2.example.configs;

import lombok.extern.slf4j.Slf4j;
import module2.example.servlets.EmbeddedServlet;
import org.springframework.boot.web.servlet.ServletContextInitializer;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRegistration;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

import static java.util.stream.Collectors.toList;


@Slf4j

public class EmbeddedWebInitializer implements ServletContextInitializer {


    private List<String> loadPropertiesUrls() throws IOException {
        Properties properties = new Properties();
        properties.loadFromXML(new FileInputStream("urls_data.xml"));

        List<String> names = properties.stringPropertyNames()
                .stream()
                .filter(n -> n.startsWith("urls"))
                .collect(toList());

        return names.stream().map(n ->properties.getProperty(n))
                .collect(toList());
    }

    private void addServlet(List<String> urls, ServletContext servletContext) {

        urls.stream()
                .forEach(u ->
                {
                    EmbeddedServlet servlet = new EmbeddedServlet();
                    ServletRegistration.Dynamic dServletRegistration = servletContext.addServlet(u, servlet);
                    dServletRegistration.addMapping("/"+u);
                    log.info("Url mapped "+"/"+u);
                });
    }

    @Override
    public void onStartup(ServletContext servletContext) throws ServletException {
        log.info("Context of Mock Server was initialized");

        try {
            addServlet(loadPropertiesUrls(),servletContext);
        } catch (IOException e) {
            log.error(Arrays.toString(e.getStackTrace()));
        }

    }
}
