package module2.example.configs;

import module2.example.servlets.CustomContainerServlet;
import org.apache.catalina.ContainerServlet;
import org.springframework.boot.autoconfigure.web.WebMvcAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
//import org.springframework.boot.context.embedded.ServletContextInitializer;
import org.springframework.boot.web.servlet.ServletContextInitializer;
/**
 * Created by kefirkb on 11.01.2017.
 */

@EnableWebMvc
@Configuration
@ComponentScan
public class AutoConfig extends WebMvcAutoConfiguration {

    @Bean
    ContainerServlet getContainer() {
        return new CustomContainerServlet();
    }

    @Bean
    ServletContextInitializer getEmbeddedWebInitializer() {
        return new EmbeddedWebInitializer();
    }
}
